def vowel_swapper(string):
    # ==============
    # Your code here
     second_instance_to_swap = 2
     return string.replace('O', '000', second_instance_to_swap).replace('000', 'O', second_instance_to_swap - 1).replace('o', 'ooo', second_instance_to_swap).replace('ooo', 'o', second_instance_to_swap - 1).lower().replace('e', '3', second_instance_to_swap).replace('3', 'e', second_instance_to_swap - 1) .replace('i', '!', second_instance_to_swap).replace('!', 'i', second_instance_to_swap - 1).replace('u', '\\/', second_instance_to_swap).replace('\\/', 'u', second_instance_to_swap - 1).replace('a', '/\\', second_instance_to_swap).replace('/\\', 'a', second_instance_to_swap - 1):
     
     

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
